//http://api.openweathermap.org/data/2.5/forecast?q=Mumbai&units=metric&APPID=d6019f272adc314922d9be4a7f67527c

const key = "d6019f272adc314922d9be4a7f67527c";

const getForecast = async (city) => {
    const base = "http://api.openweathermap.org/data/2.5/forecast";
    //const query = =Mumbai&units=metric&APPID=d6019f272adc314922d9be4a7f67527c";
    const query =  `?q=${city}&units=metric&APPID=${key}` ;
    
    const response = await fetch(base+query);
    //console.log(response);
    if(response.ok){
        const data = await response.json();
        return data;
    }else{
        throw new Error('Error Status: ' + response.status);
    }
}

/*getForecast()
    .then(data => console.log(data)) 
    .catch(err => console.log(err));*/